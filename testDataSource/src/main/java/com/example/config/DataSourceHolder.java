package com.example.config;

/**
 * 线程安全类：使用ThreadLocal存储切换数据源后的KEY
 */
public class DataSourceHolder {
    /**
     * 线程ThreadLocal
     */
    private static final ThreadLocal<String> dataSources = new InheritableThreadLocal();

    /**
     * 设置数据源
     * @param datasource
     */
    public static void setDataSource(String datasource) {
        dataSources.set(datasource);
    }

    /**
     * 获取数据源
     * @return
     */
    public static String getDataSource() {
        return dataSources.get();
    }

    /**
     * 清除数据源
     */
    public static void clearDataSource() {
        dataSources.remove();
    }

}
