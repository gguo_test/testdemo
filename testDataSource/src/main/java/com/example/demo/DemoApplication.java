package com.example.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 测试SpringBoot动态切换数据源
 */
@SpringBootApplication(scanBasePackages = "com.example.*")
@MapperScan(basePackages = { "com.example.dao" })
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
