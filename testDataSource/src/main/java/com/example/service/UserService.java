package com.example.service;

import com.example.dao.OrderMapper;
import com.example.dao.SysUserMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private OrderMapper orderMapper;

    public void addUser(String name, Integer age) {
        sysUserMapper.addUser(name, age);
    }

    public void addOrder(Double amount, String address){
        orderMapper.addOrder(amount, address);
    }
}
