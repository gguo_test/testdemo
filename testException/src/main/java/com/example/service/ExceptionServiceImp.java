package com.example.service;

import com.example.util.MasterException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ExceptionServiceImp {

    public Map test(String name) {
        if (name.equals("guoguo")) {
            throw new MasterException("名称错误");
        }
        Map result = new HashMap<>();
        result.put("userId", name);
        return result;
    }

    public int test2(int a,int b) {
        return a / b;
    }
}
