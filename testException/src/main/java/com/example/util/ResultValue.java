package com.example.util;


import java.io.Serializable;
import java.util.List;

/**
 * 返回结果
 *
 * @author
 */
public class ResultValue<T> {


    /**
     * 异常信息，单行信息
     */
    private String message;

    /**
     * 信息提示类型
     */
    private String messageType = "error";

    /**
     * 数据
     */
    private T data;

    /**
     * 执行是否成功
     */
    private boolean success;

    /**
     * 构造函数
     */
    public ResultValue() {
    }

    /**
     * 构造函数
     *
     * @param data    数据
     * @param success 是否成功
     * @param message 异常/提示信息
     */
    public ResultValue(T data, boolean success, String message) {
        this.data = data;
        this.success = success;
        this.message = message;
        if (success) {
            this.messageType = null;
        }
    }

    /**
     * 默认成功的构造函数
     *
     * @param data 数据
     */
    public ResultValue(T data) {
        this.data = data;
        this.success = true;
        this.messageType = null;
    }

    /**
     * 获得返回的信息
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageType() {
        return messageType;
    }

    /**
     * @return the data
     */
    public T getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(T data) {
        this.data = data;
    }

    /**
     * @return the succeed
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * @param success the succeed to set
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }
}
