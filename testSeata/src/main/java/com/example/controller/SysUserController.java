package com.example.controller;

import com.example2.client.ISysUserClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class SysUserController {

    @Autowired
    private ISysUserClient sysUserClient;

    @Autowired
    private SysUserService sysUserService;

    @GetMapping(value = "/sysUser/addUser")
    public String addUser(String name, Integer age, Double amount, String address) {
        return sysUserService.addUser(name, age, amount, address) ? "success" : "fail";
    }
}