package com.example.controller;

import com.example2.client.ISysUserClient;
import com.example.dao.SysUserMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class SysUserService {
    @Autowired
    private SysUserMapper userMapper;
    @Autowired
    private ISysUserClient sysUserClient;
    private static final Logger logger = LoggerFactory.getLogger(SysUserService.class);

    @GlobalTransactional
    public Boolean addUser(String name, Integer age, Double amount, String address) {
        logger.info("addUser xid:{}", RootContext.getXID());

        // 操作sysUser
        int i = userMapper.addUser(name, age);
        // 操作consoleUser
        int j = sysUserClient.addOrder(amount, address);
//

        // 测试事务回滚(age = 0：回滚；age > 0：事务提交)
        int flag = 1 / age;

        return i > 0 && j > 0;
    }

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {

        Maps.newHashMapWithExpectedSize(10);
        HashMap<Object, Object> map = Maps.newHashMap();
        map.put("1","1");
//        ArrayList<Object> objects = Lists.newArrayList();
//
//        for (int i=1;i<=18;i++){
//            objects.add(i);
//        }

//        Lists.newArrayListWithCapacity(1);
//        List<String> myList = Lists.newArrayListWithExpectedSize(5);
//        // 使用反射解除访问限制
//        Field field = ArrayList.class.getDeclaredField("elementData");
//        field.setAccessible(true);
//        Object[] elementData = (Object[]) field.get(myList);
//        int capacity = elementData.length;
//        System.out.println("ArrayList 的容量为：" + capacity);

    }

}
