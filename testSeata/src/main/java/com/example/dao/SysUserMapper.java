package com.example.dao;

import feign.Param;
import org.apache.ibatis.annotations.Insert;

public interface SysUserMapper {
    @Insert("INSERT INTO user VALUES (NULL, #{name}, #{age}, 1, NOW(), NOW())")
    int addUser(@Param("name") String name, @Param("age") Integer age);
}
