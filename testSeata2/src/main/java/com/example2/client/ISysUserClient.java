package com.example2.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(name = "masterdata",url = "${masterdata-service-url}")
public interface ISysUserClient {

    @PostMapping(value = "/masterdata/addOrder")
    public int addOrder(@RequestParam("amount")Double amount, @RequestParam("address")String address);
}
