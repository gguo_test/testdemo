package com.example2.service;

import com.example2.dao.OrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class OrderServiceImpl {
    @Autowired
    private OrderMapper orderMapper;

    public int addOrder(Double amount,String address){
        int count = orderMapper.addOrder(amount, address);
       int i = count / 0;
        return count;
    }
}
